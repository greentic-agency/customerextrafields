# Module "customerextrafields" d'ajout de nouveaux champs client

### **Nouveaux champs dans le formulaire d'inscription client**
*   __Lien__ : /connexion?create_account=1
*   __Fichiers TPL pour la recherche ajax de l'identifiant__ : /modules/customerextrafields/src/views/templates/hook/hookDisplayCustomerAccountForm.tpl<br>
__Réponse Ajax lors de la recherche d'un login__  : /modules/customerextrafields/src/views/templates/ajax/ajax.tpl
__Fichier JS__ : /modules/customerextrafields/src/views/js/search-login.js

Les nouveaux champs sont ajoutés automatiquement en php par le Prestashop.

### **Widget de mise à jour du login**
*   __Lien__ : /mon-compte
*   __Fichier TPL__ : /modules/customerextrafields/src/views/templates/widget/login-update.tpl

>Appelé dans le fichier TPL du module "itou" : /modules/itou/views/templates/front/my-account/menu/links.tpl

Shortcode : {widget name="customerextrafields" }

On peut bien évidemment le mettre partout




