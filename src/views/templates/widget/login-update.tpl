
<form class="form-inline">
    <div class="form-group mb-2">
        <label class=''>{l s='Mon identifiant'}
        <input name='id' type='hidden' value='{$customer_id}'>
        <input class='form-control' name='login' type='text' value='{$login}'>
        <button class='btn btn-primary' type='submit' data-link-action='save-customer' data-action='editLogin'>
            {l s='Mettre à jour'}
        </button>

        <div id='edit-notification'></div>

        {include file='module:customerextrafields/src/views/templates/hook/hookDisplayCustomerAccountForm.tpl'} 
    </div>
</form>
