<div class='col-md-12'>
  <div id='result-login'></div>
  <div id='alert-login' class='hidden-xl-down'>
    <div class="alert alert-danger" role="alert">
        <strong>{l s='Vous devez remplir un identifiant unique d\'au moins 4 caractères.'}
    </div>
  </div>
</div>