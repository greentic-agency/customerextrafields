
{if $result}
    <div class='alert alert-danger' role='alert' data-login='error'>
        <strong>{l s='Oh mince !'}</strong> {l s='Cet identifiant est déjà utilisé.'}
    </div>
{else}
    <div class='alert alert-success' role='alert' data-login='success'>
        <strong>{l s='Ok!'}</strong>
    </div>
{/if}