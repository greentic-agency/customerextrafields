$(document).ready(function () 
{

    $("input[name=login]").on('keyup', delay(function(e) {
        testLogin();
    }, 400));


    $('button[data-link-action=save-customer]').on('click', function() {

        var login = $('input[name=login]').val();
        var loginExists = $('div.alert').attr('data-login');

        if(login == '' || login.length < 4 || loginExists == 'error') {
            $('#alert-login').removeClass('hidden-xl-down');
            return false;
        }else{
          
            $('#alert-login').addClass('hidden-xl-down');

            if($(this).attr('data-action') == 'editLogin'){
                update();
            }
        }
    });

});

/**
 * Send request after a given time
 * @param callback
 * @param ms
 * 11-14-2018
 */
function delay(callback, ms) 
{
    var timer = 0;
    return function() {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        callback.apply(context, args);
      }, ms || 0);
    };
  }

/**
 * Test if login exists in database
 * @param bool onload
 * 11-14-2018
 */
function testLogin(onload = false)
{  
    var script_search = 'index.php?fc=module&module=customerextrafields&controller=ajax&searchLogin=1';
    var login = $('input[name=login]').val();

     if(typeof login != 'undefined' && login != '' && login.length >= 4) {

        $.ajax({
            url : script_search,
            data : {
                login : login
            },
            dataType : 'html',
            success : function(html){
                if(!onload)
                    $('#result-login').html(html);
                    
                $('#alert-login').addClass('hidden-xl-down');
            }
        });
     } else {
         $('#result-login').html('');
         $('#alert-login').removeClass('hidden-xl-down');
    }

    return false;
}

/**
 * Update Login of customerextrafields model
 */
function update()
{
    var script_edit = 'index.php?fc=module&module=customerextrafields&controller=ajax&editLogin=1';
    var login = $('input[name=login]').val();
    var id = $('input[name=id]').val();

    $.ajax({
        url : script_edit,
        data : {
            login : login,
            id : id
        },
        dataType : 'html',
        success : function(html){
            $("#edit-notification").show();
            $("#edit-notification").html(html);
            $("#edit-notification").fadeOut(2000);
            $('#result-login').addClass('hidden-xl-down');
        }
    });

}