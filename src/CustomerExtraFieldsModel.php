<?php

namespace Modules\Itou\CustomerExtraFields;

use ObjectModel;
use Db;
use DbQuery;
use Tools;

class CustomerExtraFieldsModel extends ObjectModel
{
    const _TABLE_MODULE_ = 'customerextrafields';

     /** @var string Iban */
     public $iban;

     /** @var string Bic code */
     public $bic_code;
 
     /** @var bool Status */
     public $is_noticed_by_text_message;
 
     /** @var string City */
     public $city;
 
     /** @var string Zip code*/
     public $zip_code;
 
    /** @var string Phone */
    public $phone;

    /** @var string Login */
    public $login;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'customerextrafields',
        'primary' => 'id',
        'multilang' => false,
        'fields' => array(
            'iban' => array('type' => self::TYPE_STRING, 'size' => 27, 'validate' => 'isGenericName', 'type_database' => 'VARCHAR', 'subscription_form' => false),
            'bic_code' => array('type' => self::TYPE_STRING, 'size' => 11, 'validate' => 'isGenericName', 'type_database' => 'VARCHAR','subscription_form' => false),
            'is_noticed_by_text_message' => array('type' => self::TYPE_BOOL, 'size' => 1, 'validate' => 'isBool', 'type_database' => 'TINYINT', 'subscription_form' => false),
            'city' => array('type' => self::TYPE_STRING, 'size' => 64, 'validate' => 'isCityName', 'type_database' => 'VARCHAR','subscription_form' => true),
            'zip_code' => array('type' => self::TYPE_STRING, 'required' => false, 'validate' => 'isPostCode', 'size' => 12, 'type_database' => 'VARCHAR', 'subscription_form' => true),
            'phone' => array('type' => self::TYPE_STRING,  'required' => false, 'validate' => 'isPhoneNumber', 'size' => 32, 'type_database' => 'VARCHAR', 'subscription_form' => true),
            'login' => array('type' => self::TYPE_STRING, 'size' => 64, 'type_database' => 'VARCHAR', 'subscription_form' => true),
        ),
    );

      /**
     * Builds the object
     *
     * @param int|null $id If specified, loads and existing object from DB (optional).
     * @param int|null $id_lang Required if object is multilingual (optional).
     * @param int|null $id_shop ID shop for objects with multishop tables.
     * @param PrestaShopBundle\Translation\Translator
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function __construct($id = null, $idLang = null)
    {
        parent::__construct($id, $idLang);
        $this->force_id = true;
    }

  
    /**
     * Saves current object to database (add or update)
     *
     * @param bool $null_values
     * @param bool $auto_date
     *
     * @return bool Insertion result
     * @throws PrestaShopException
     */
    public function save($null_values = true, $auto_date = true)
    {
        if(!CustomerExtraFieldsModel::existsInDatabase($this->id, self::_TABLE_MODULE_)) {
            $this->is_noticed_by_text_message = true;
            return $this->add($auto_date, $null_values);
        }else{
            return $this->update($null_values);
        }
    }

    /**
     * Checks if an object exists in database.
     *
     * @param int    $id_entity
     * @param string $table
     *
     * @return bool
     */
    public static function existsInDatabase($id_entity, $table)
    {
        $row = Db::getInstance()->getRow('
			SELECT `id` as id
			FROM `'._DB_PREFIX_.bqSQL($table).'` e
			WHERE e.`id` = '.(int)$id_entity, false
        );
        return isset($row['id']);
    }

    /**
     * Add or update datas to database
     * @param int $id_object
     * @return bool
     */
    public static function saveNewFieldsRow($id_object)
    {
        $datas = Tools::getAllValues();

        if(array_key_exists('login', $datas)) {
            $customerExtraFields = new CustomerExtraFieldsModel();
            $customerExtraFields->id = $id_object;
            foreach($customerExtraFields->getFields() as $key => $value) {
                if($key != "id"){
                    if(array_key_exists($key, $datas)) {
                        $customerExtraFields->$key = $datas[$key];
                    } else {
                        $customerExtraFields->$key = false;
                    }
                }
            }
            return $customerExtraFields->save(true, true);
        }

        return true;
    }

    /**
     * Get values from database
     * @param int $idCustomer 
     * @return array
     */
    public function getValues(int $idCustomer)
    {
        $query = (new DbQuery())
        ->select('*')
        ->from(self::_TABLE_MODULE_, 'c')
        ->where('c.id = ' . pSQL($idCustomer));

        if(Db::getInstance()->getRow($query))
            return Db::getInstance()->getRow($query);
        else
            return [];
    }
}