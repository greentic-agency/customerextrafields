<?php

namespace Modules\Itou\CustomerExtraFields\Module;

use Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel as CustomerExtraFieldsModel;
use Db;

trait Installable
{
    /**
     * Module installation
     * @return BOOL
     */
    public function install() 
    {
        if($success = parent::install()
            && $this->installModuleTables()) {
                foreach ($this->hooks as $hook) {
                    $success = $success && $this->registerHook($hook);
                }
            }
        return $success;
    }

    /**
     * Module uninstallation
     * @return BOOL
     */
    public function uninstall() 
    {
        if($success = parent::uninstall() 
            && $this->unInstallModuleTables()) {
                foreach ($this->hooks as $hook) {
                    $success = $success && $this->unregisterHook($hook);
                }
            }
            return $success;
    }

    /**
     * Module uninstall tables
     * @return BOOL
     */
    public function unInstallModuleTables()
    {
        return Db::getInstance()->execute(
			'DROP TABLE IF EXISTS
			`'._DB_PREFIX_.self::_TABLE_MODULE_.'`'
		);
    }

    /**
     * Install the module's table
     * @return bool
     */
    public function installModuleTables()
    {
        $customerExtraFields = new CustomerExtraFieldsModel();

        $newFields = $customerExtraFields->getDefinition($customerExtraFields)['fields'];
        $fields = $this->createRequestLineNewField($newFields);
       
        return Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::_TABLE_MODULE_.'` 
            (`id` INT(11) NOT NULL AUTO_INCREMENT, '.
            $fields. ',
            PRIMARY KEY (`id`)) DEFAULT CHARSET=utf8'
        );
    }

    /**
     * Rewrite line with array datas for the creating module table request
     * @return string
     */
    public function createRequestLineNewField(array $newFields = [])
    {
       $lines = [];
        foreach($newFields as $name => $newFieldParams) {
            $newFieldParamsFormated = $this->formateFieldType($newFieldParams['type'], $name);
            $lines[] = $name . " " . 
                        $newFieldParams['type_database'] .
                        '(' .$newFieldParams['size'] . ")" .
                        (array_key_exists('default', $newFieldParams) ?
                        ' DEFAULT ' . $newFieldParams['default'] :
                        '');
        }

        $line = implode(', ', $lines);
        return $line;
    }

}