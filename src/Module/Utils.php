<?php

namespace Modules\Itou\CustomerExtraFields\Module;
Use Db;
Use DbQuery;
Use Context;

trait Utils
{
     /**
     * @params int $idType
     * @return string
     */
    public function formateFieldType(int $idType, string $name)
    {
        $type = array();
        switch ($idType) {
            case 2 : 
                $type['type'] = 'switch';
                $type['type_form_front'] = 'radio-buttons';
                $type['value_form_front'] = array(true => $this->l('Yes'), false => $this->l('No'));
                $type['is_bool'] = true;
                $type['value'] = array(
                    array(
                        'id' => $name.'_on',
                        'value' => 1,
                        'label' => $this->trans('Enabled', array(), 'Admin.Global')
                    ),
                    array(
                        'id' => $name.'_off',
                        'value' => 0,
                        'label' => $this->trans('Disabled', array(), 'Admin.Global')
                    )
                );
                break;
            default :
                $type['type'] = 'text';
                $type['type_form_front'] = 'text';
                $type['is_bool'] = false;
                $type['value'] = '';
                $type['value_form_front'] = '';
                
                break;
        }

        return $type;
    }

    /**
     * Format fields to display value properly
     * @param string $FieldName to formate
     * @return string
     */
    public function formateFieldName(string $fieldName)
    {
        return  ucfirst(str_replace("_", " ", $fieldName));
    }

     /**
     * Fill current object with new fields
     * @param array $newFieldsValue
     * @param array $params
     * @return object
     */
    public function populateObject(array $newFieldsValue, $params)
    {
        foreach($newFieldsValue as $key => $newFieldValue){
            $params['object']->$key = $newFieldValue;
        }
    }

    /**
     * Check if field is empty in database
     * @param string $field
     * @param string $table
     * @param string $where
     * @param string $where_value
     * @param bool $return_value
     * @param bool $customer_id
     * @return bool
     */
    public function checkField($field, $table, $where, $where_value, $return_value = false, $customer_id = false){

        $andWhere = '';
        if($customer_id) {
            $andWhere = ' AND c.id <> '. Context::getContext()->customer->id;
        }

        $count = (new DbQuery())
        ->select($field)
        ->from($table, 'c')
        ->where('c.'.$where.' = "' . pSQL($where_value) . '"' . $andWhere);
        
        if($result = Db::getInstance()->getRow($count)) {
           if(!is_null($result[$field]) && !empty($result[$field])){
                if($return_value)
                    return $result[$field];
                else
                    return true;
           }
        }
        return false;
    }

    /**
     * Remove taxes from Price
     * @param $price_without_taxes
     */
    public function calculatePriceWT($price_without_taxes, $taxe_percentage)
    {
        $price_with_taxes = $price_with_taxes / (1 + ($taxe_percentage/100));
        return $price_with_taxes;
    }
}