<?php

namespace Modules\Itou\CustomerExtraFields;

use Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel;
use Modules\Itou\CustomerExtraFields\Module\Configurable;
use Modules\Itou\CustomerExtraFields\Module\Installable;
use Modules\Itou\CustomerExtraFields\Module\Utils;

use Module as PrestashopModule;
use Tools;
use Context;
use DbQuery;
use Db;
use FormField;
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Module extends PrestashopModule implements WidgetInterface
{
    const _TABLE_MODULE_ = 'customerextrafields';
    
    use Configurable;
    use Installable;
    use Utils;

    protected $hooks = [
        'actionAdminCustomersFormModifier',
        'additionalCustomerFormFields',
        'actionObjectCustomerAddAfter',
        'actionObjectCustomerUpdateAfter',
        'displayCustomerAccountForm',
        'validateCustomerFormFields',
        'actionObjectCustomerDeleteAfter',
        'header',
        'actionCustomerAccountAdd',
        'afterCustomerAddressFormGetTemplateVariables'
    ];

    protected $path_script = "customerextrafields/src/views/";

    /**
     * Redirect to my account after registration
     */
    public function hookActionCustomerAccountAdd()
    {
        Tools::redirect('index.php?controller=my-account');
    }

    /**
     * Fill address with values of ps_customerextrafields
     * for displaying on front address form
     * @param array $addressFields
     */
    public function hookAfterCustomerAddressFormGetTemplateVariables(array $addressFields)
    {
        $id_customer = Context::getContext()->customer->id;
        $customerExtraFields = new CustomerExtraFieldsModel($id_customer);

        if (empty( $addressFields['addressFields']['formFields']['postcode']['value'])) {
            $addressFields['addressFields']['formFields']['postcode']['value'] = $customerExtraFields->zip_code;
        }
        if (empty( $addressFields['addressFields']['formFields']['city']['value'])) {
            $addressFields['addressFields']['formFields']['city']['value'] = $customerExtraFields->city;
        }
        if (empty( $addressFields['addressFields']['formFields']['phone_mobile']['value'])) {
            $addressFields['addressFields']['formFields']['phone_mobile']['value'] = $customerExtraFields->phone;
        }
    }

    /**
     * AddJs
     */
    public function hookDisplayHeader($params){
        $this->context->controller->addJS(_MODULE_DIR_ . $this->path_script . 'js/search-login.js');
    }

    /**
     * Hook update datas to database - Front Office
     * @param type $params
     */
    public function hookActionObjectCustomerUpdateAfter(array $params)
    {
        CustomerExtraFieldsModel::saveNewFieldsRow($params['object']->id);
    }

    /**
     * Hook add datas to database from registration account - Front Office
     * @param type $params
     */
    public function hookActionObjectCustomerAddAfter(array $params) 
    {
        CustomerExtraFieldsModel::saveNewFieldsRow($params['object']->id);
    }

    /**
     * Delete datas from module table when customer is deleted
     * @param type $params
     */
     public function hookActionObjectCustomerDeleteAfter(array $params)
     {  
        $customerExtraFields = new CustomerExtraFieldsModel(Tools::getValue('id_customer'));
        return $customerExtraFields->delete();
     }

    /**
     * Display new fields in customer form - Back office 
     * @param type $params
     */
    public function hookActionAdminCustomersFormModifier(array $params) 
    {
        $customerExtraFieldsClass = new CustomerExtraFieldsModel();

        $newFieldsValue = $customerExtraFieldsClass->getValues(Tools::getValue('id_customer'));
        $this->populateObject($newFieldsValue, $params);

        $newFields = $customerExtraFieldsClass->getDefinition($customerExtraFieldsClass)['fields'];
        
        foreach($newFields as $name => $value) {
            if($name != "id_customer") {
                $type = $this->formateFieldType($value['type'], $name);

                $params['fields'][0]['form']['input'][] = [
                                    'type' => $type['type'],
                                    'label' => $this->l($this->formateFieldName($name)),
                                    'name' => $name,
                                    'class' => 't',
                                    'is_bool' => $type['is_bool'],
                                    'hint' => $this->l($this->formateFieldName($name)),
                                    'values' => $type['value']
                                ]; 
                
                $params['fields_value'][$name] = (property_exists($params['object'], $name) ?
                                                 $params['object']->$name :
                                                 "");
            }
        }
    }

    /**
     * Add the new fields on account customer AUTO - Front Office
     * @param type $params
     * @return array
     */
    public function HookAdditionalCustomerFormFields(array $params)
    {
        $controller = Tools::getValue('controller');
        $id_customer = Context::getContext()->customer->id;
        $newFieldsForm = [];
        
        $customerExtraFields = new CustomerExtraFieldsModel($id_customer);

        $newFields = $customerExtraFields->getDefinition($customerExtraFields)['fields'];
     
        foreach($newFields as $name => $newFieldParams) {
            if(($newFieldParams['subscription_form'] && $controller == "authentication") 
                || $controller == "identity") {

                $type = $this->formateFieldType($newFieldParams['type'], $name);

                   if($type['type_form_front'] == 'radio-buttons')
                    $value = 'setAvailableValues';
                else
                    $value = 'setValue';

                $newFieldsForm[] = (new FormField)
                                ->setName($name)
                                ->setType($type['type_form_front'])
                                ->setLabel($this->l($this->formateFieldName($name)))
                                ->$value(!empty($type['value_form_front']) ? $type['value_form_front'] : $customerExtraFields->$name)
                                ->setValue($customerExtraFields->$name);
            }
        }

        return $newFieldsForm;
    }

    /**
     * Display new fields in customer form - Front Office 
     * @param type $params
     */
    public function hookDisplayCustomerAccountForm(array $params)
    {
        //return $this->display(_PS_MODULE_DIR_.'customerextrafields/customerextrafields.php', 'src/views/templates/hook/hookDisplayCustomerAccountForm.tpl'); 
    }

    /**
     * Display widget
     * @param type $hookName
     * @param array $configuration : All the variables
     * @return array
     */
    public function renderWidget($hookName = null, array $configuration = []) 
    {
        if ($this->context->customer->logged === true) {
            $this->smarty->assign($this->getWidgetVariables($hookName, $configuration));
            return $this->display(_PS_MODULE_DIR_.'customerextrafields/customerextrafields.php', 'src/views/templates/widget/login-update.tpl'); 
        }
    }
 
    /**
     * Get widget's variable
     * @param type $hookName
     * @param array $configuration
     * @return array
     */
    public function getWidgetVariables($hookName = null , array $configuration = [] )
    {
        $customer = new CustomerExtraFieldsModel(Context::getContext()->customer->id);

        return  [
            'login' => $customer->login,
            'customer_id' => $customer->id
            ];
 
    }

}