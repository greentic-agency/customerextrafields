<?php
class CustomerAddressForm extends CustomerAddressFormCore
{
    public function getTemplateVariables()
    {
        $addressFields = parent::getTemplateVariables();
        Hook::exec(
            sprintf('after%s%s', get_called_class(), ucfirst(__FUNCTION__)),
            [
                'addressFields' => &$addressFields
            ]
        );
        return $addressFields;
    }
}