<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once __DIR__ . '/autoload.php';

class CustomerExtraFields extends Modules\Itou\CustomerExtraFields\Module
{
    /**
     * Module construct
     */
    public function __construct()
    {
        $this->name = 'customerextrafields';
        $this->author = 'Lucile Greentic';
        $this->version = '1.0';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('I-tou - Customer Extra Fields');
        $this->description = $this->l('Displays and adds customer extra fields.');
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
    }
}