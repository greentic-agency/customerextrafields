<?php

use Modules\Itou\CustomerExtraFields\Module\Utils;
use Modules\Itou\CustomerExtraFields\Module\Upload;
use Modules\Itou\CustomerExtraFields\CustomerExtraFieldsModel as CustomerExtraFieldsModel;
/**
 * Itou
 * Scripts ajax
 */
class customerextrafieldsajaxModuleFrontController extends ModuleFrontController
{ 
    /**
	 * Init Content
	 */
	public function initContent()
	{
		parent::initContent();
		$context = Context::getContext();

        if (Tools::getIsset('searchLogin') && $login = Tools::getValue('login')) {
            $this->searchLogin($login);
            $this->setTemplate('module:customerextrafields/src/views/templates/ajax/ajax.tpl');
        }elseif(Tools::getIsset('editLogin')) {
            $this->editLogin();
            $this->setTemplate('module:customerextrafields/src/views/templates/ajax/edit-notification.tpl');
        }

    }
    
    /**
     * Search if buyer exists to link him to the new product
     * @params string $login
     */
    public function searchLogin(string $login)
    {
        $context = Context::getContext();
        $result = false;
        $customer_id = isset($context->customer->id) ? true : false;
          
        if(Utils::checkField('id', 'customerextrafields', 'login', $login, true, $customer_id)) {
            $result = true;
        }

        $this->context->smarty->assign(
            array(
                'result' => $result
            )
        );

        return true;
    }

    /**
     * Edit Login
     */
    public function editLogin()
    {
        $login = Tools::getValue('login');
        $customer_id = Tools::getValue('id');

        $customer = new CustomerExtraFieldsModel($customer_id);
        $customer->login = pSQL($login);
        $customer->update(true, true);

    }
}