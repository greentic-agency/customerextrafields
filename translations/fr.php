<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{customerextrafields}prestashop>customerextrafields_64ed53bfbe5b4400dae849baa6b92386'] = 'I-tou - Nouveaux champs clients';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_e90f149a7e30ee4e4099f5b6764ab0d1'] = 'Affiche et ajouter des nouveaux champs clients.';
$_MODULE['<{customerextrafields}prestashop>utils_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{customerextrafields}prestashop>utils_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_bcc254b55c4a1babdf1dcb82c207506b'] = 'Téléphone';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_99dea78007133396a7b8ed70578ac6ae'] = 'Identifiant';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_642d3ba5db8b57e006584b544e490ec7'] = 'Code postal';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_57d056ed0984166336b7879c2af3657f'] = 'Ville';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_814b691f27f5022a1a73b5131fd82218'] = 'BIC';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_b3abe0fc9c65c7485f3e468fac4f342b'] = 'IBAN';
$_MODULE['<{customerextrafields}prestashop>customerextrafields_83763298bd36e00b903e9e1e12d9b12f'] = 'Je préfère être alerté par SMS';